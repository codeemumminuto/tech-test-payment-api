using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Context
{
    public class ConexaoContext : DbContext
    {
        public ConexaoContext(DbContextOptions<ConexaoContext> options) : base(options)
        {
            
        }

        public DbSet<Venda> Vendas{ get; set; }
    }
}