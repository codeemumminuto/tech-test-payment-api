using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly ConexaoContext _context;
        
        public VendaController(ConexaoContext context)
        {
            _context = context;
        }

        [HttpPost("RegistrarVenda")]
        public IActionResult Registrar(Venda venda)
        {
            if (!venda.ItensVendidos.Contains(";"))
            {
                return BadRequest(new { Erro = "Adicione um \";\" ao fim de cada item" });
            }

            venda.Status = "Aguardando Pagamento";

            _context.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(BuscarVendaPorId), new { id = venda.Id }, venda);
        }

        [HttpGet("BuscarVendaPorId/{id}")]
        public IActionResult BuscarVendaPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            return venda == null ? NotFound() : Ok(venda);
        }

        [HttpPut("AtualizarVenda/{id}")]
        public IActionResult AtualizarVenda(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (venda.Status.ToLower() != "aguardando pagamento" && venda.Status.ToLower() != "pagamento aprovado" && venda.Status.ToLower() != "cancelada" && venda.Status.ToLower() != "enviado para Transportadora" && venda.Status.ToLower() != "entregue")
            {
                return BadRequest(new { Erro = "Escolha um status válido (Aguardando pagamento, Pagamento Aprovado, Cancelada, Enviado para Transportadora, Entregue)" });
            }

            var statusbanco = vendaBanco.Status.ToLower();
            var statuspreenchido = venda.Status.ToLower();

            if (statusbanco == "aguardando pagamento" && (statuspreenchido != "pagamento aprovado" && statuspreenchido != "cancelada"))
            {
                return BadRequest(new { Erro = "Aguardando pagamento só pode ser alterado para Pagamento aprovado ou Cancelada." });
            }
            else if (statusbanco == "pagamento aprovado" && (statuspreenchido != "enviado para transportadora" && statuspreenchido != "cancelada"))
            {
                return BadRequest(new { Erro = "Pagamento aprovado só pode ser alterado para Enviado para transportadora ou Cancelada." });
            }
            else if (statusbanco == "enviado para transportadora" && statuspreenchido != "Entregue")
            {
                return BadRequest(new { Erro = "Enviado para transportadora só pode ser alterado para Entregue." });
            }
            else if (statusbanco == "cancelada" || statusbanco == "entregue")
            {
                return BadRequest(new { Erro = "Não é possível alterar um registro com status Cancelada ou Entregue." });
            }
            else
            {
                if (venda.IdVendedor != null)
                {
                    vendaBanco.IdVendedor = venda.IdVendedor;
                }

                if (venda.CpfVendedor != null)
                {
                    vendaBanco.CpfVendedor = venda.CpfVendedor;
                }

                if (venda.NomeVendedor != null)
                {
                    vendaBanco.NomeVendedor = venda.NomeVendedor;
                }

                if (venda.EmailVendedor != null)
                {
                    vendaBanco.EmailVendedor = venda.EmailVendedor;
                }

                if (venda.TelefoneVendedor != null)
                {
                    vendaBanco.TelefoneVendedor = venda.TelefoneVendedor;
                }

                if (venda.ItensVendidos != null)
                {
                    vendaBanco.ItensVendidos = venda.ItensVendidos;
                }

                if (venda.Status == null)
                {
                    return BadRequest(new { Erro = "Adicione um status para atualizar" });
                }

                vendaBanco.Status = venda.Status;
                _context.Vendas.Update(vendaBanco);
                _context.SaveChanges();

                return vendaBanco == null ? NotFound() : Ok(vendaBanco);
            }
        }
    }
}